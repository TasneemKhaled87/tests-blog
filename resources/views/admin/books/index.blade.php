@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Books:</div>
<p>

                <div class="card-body">
           

                <table class="table table-borderless table-dark">
                       <thead>
                          <tr>
                              <th scope="col-2">No.of.books</th>
                              <th scope="col">cover</th>
                              <th scope="col">category</th>
                              <th scope="col">publishedat</th>
                              <th scope="col">Edit</th>
                              <th scope="col">Delete</th>
 
                          </tr>
                     </thead>
                     <tbody>
                        @foreach ($books as $book)
                         <tr>
                              <th scope="row">{{$book->title}}</th>
                              <th scope="row"> </th>
                              <th scope="row">{{$book->category_id}}</th>
                              <th scope="row">{{$book->published_at}}</th>

                               <td>
                               <a class="" href="{{route('book.edit',['id'=>$book->id])}}"><i class="fas fa-edit"></li>Edit</a>

                               </td>

                               <td>
                               <a class="" href="{{route('book.delete',['id'=>$book->id])}}"><i class="far fa-trash-alt"></i>Delete</a>

                               </td>
                         </tr>
                        @endforeach
                     </tbody>
                </table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
