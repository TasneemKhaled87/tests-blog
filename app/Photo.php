<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    public function books()
    {
        return $this->belongsTo('App\Book');
    }
    public function foobar()
    {
        return 'fbr';
    }
}
